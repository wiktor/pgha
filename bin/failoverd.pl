#!/usr/bin/perl

# failoverd.pl 
# Version: 9.2-1.00.30

# Authors / Maintainers :
# Scott Mead, scottm@openscg.com -- SRM
# Fahd Najmi, fahdn@openscg.com -- FN
# Venkata B Nagothi, venkata.nagothi@openscg.com -- VBN
# Farrukh Afzal, farrukha@openscg.com -- FA

# Copyright OpenSCG (2009-2013) 
# License: GPL V3 + attribution
# The pgHA project and all of its components are release under GPL V3 + attribution.
# See COPYRIGHT for more details including a copy of the license
#

# failoverd is an automated failover daemon for use with postgres
# streaming replication 
# ================
# Revision History
# ==================================================================
#  Date      |   Author    |   Version  |      Comment      
# ==================================================================
# 2012-09-18 |    SRM      |    RC      |  RC
# 2012-09-19 |    SRM      |  RC 0.5    |  Added connection pool / master DB host validation 
#                                       |   fence commands execute in same pgbouncer connection 
# 2012-09-19 |    SRM      |  RC 0.7    |  Merged pushbutton code 
# 2012-09-22 |    SRM      |  RC 0.8    |  Added failover avoidence on slave down
# 2012-09-30 |    SRM      |  RC 0.8.5  |  Added appserver pre-fence check
#                                          Added status_table configuration option
#                                          Added status_table writes during startup / failover 
#                                          Added message about testing triggerCommand
# 2013-03-22 |    SRM      |9.2-1.00.00 |  Modified startup messages and fixed some formatting
# 2013-04-29 |    SRM      |9.2-1.00.10 |  Disarm of pgBouncers happens on failoverd.pl stop only 
#                                           Changed --start to --auto
# 2013-05-31 |    VBN      |9.2-1.00.20 |  Added nagios compliant output mode
# 2013-07-21 |    FA       |9.2-1.00.30 |  Added v|version (prints version information)
#                                          Fixed nagios output.



use Config::IniFiles;
use Date::Format;
use DBI;
use DBD::Pg;
use File::Basename;
use Getopt::Long;
use IO::Socket::INET;
use Log::Log4perl;
use Proc::Daemon;
use Net::Ping;
use Term::ANSIColor;
use Nagios::Plugin;
use Nagios::Plugin::Functions;
use Nagios::Plugin::Getopt;

my $programName="failoverd";

my $showConfig=0;
my $endWorker=0;
my $startWorker=0;
my $dryRun=0;
my $statsMode=0;
my $pushButton=0;
my $executeImmediateFailover=0;
my $version="9.2-1.00.30";

########## Main Program ############

# User specified config file
GetOptions ('c|config=s' =>\$confFile,
            'showConfig' =>\$showConfig,
            'status'     =>\$workerStatusCheck,
            'kill|stop'  =>\$endWorker,
            'auto'      =>\$startWorker,
            'dryrun'     =>\$dryRun,
            'stats'      =>\$statsMode,
            'pushButton|manual' =>\$pushButton,
            'nagiosStatus' => \$nagiosStatusCheck,
            'failover' => \$executeImmediateFailover,
            'v|version'     =>\$checkVersion,
            'h|help'     =>\$help);

# If we are missing an ACTION
if ( !$endWorker && !$workerStatusCheck && !$startWorker 
    && !$executeImmediateFailover && !$pushButton && !$nagiosStatusCheck && !$help && !$checkVersion)
{
    print "\n You must define one ACTION\n";
    print "\t --auto\n";
    print "\t --manual or --pushButton\n";
    print "\t --failover\n";
    print "\t --stop\n";
    print "\t --status\n";
    print "\t --nagiosStatus\n";
    print "\t --version\n";
    print "\t --help\n";
    $help=1;
}

if ( (!defined ($confFile) || $help) && !$checkVersion)
{
    print "\n===============\n";
    print "  $programName     ";
    print "\n===============\n";
    print "Usage: \n\n";
    print "$0 [OPTIONS] ACTION\n";    
    print "\n\n";
    print " OPTIONS:\n";
    print "\t-c | --config <config_file>\tSpecify the config file for $programName\n";
    print "\t--dryRun\tAll actions execute EXCEPT actual failover commands\n";
    print "\t--stats \tRun the system in statistics mode for capturing timings\n";
    print "\t--showConfig\tDisplays the parsed config options (debug)\n";
    print " ACTIONS:\n";
    print "\t--auto\t\tStarts the $programName-worker in automated failover mode\n";
    print "\t--manual or\n"
         ."\t--pushButton \tStarts the $programName-worker in pushButton mode ( disables auto-failover )\n";
    print "\t--failover \tInitiates a forced failover of all services to the slave DB\n";
    print "\t--stop  \tEnds the $programName-worker for the specified config file\n";
    print "\t--status\tChecks the $programName-worker for the specified config file\n";
    print "\t--nagiosStatus\tProvides nagios compliant output of current status\n";
    print "\t--version  \tPrint version info and exit\n";
    print "\t--help  \tPrint this message and exit\n";
    print "\n\n To start a $programName-worker, simply define a config file and run:\n";
    print "\t$0 -c /path/to/config.conf --start\n";
    print "\n To End a $programName-worker:\n";
    print "\t$0 -c /path/to/config.conf --stop\n";
    print "\n\n";

    exit (1);
}
 # Print failoverd Version information & exit
if (!defined ($confFile) && $checkVersion )
{
     print "\n pgHA Version : $version\n";
     exit (0);
}

my %Config;

#Config Options
my $dbname='dbname';
my $dbuser='dbuser';
my $dbpass='dbpass';
my $dbhost='dbhost';
my $dbhostUser ='dbhost_user';
my $pgbouncerDb='pgbouncer_db';
my $pgbouncerDbUser='pgbouncer_db_user';
my $pgbouncerDbPass='pgbouncer_db_pass';
my $pgbouncerConfig="pgbouncer_config";
my $pgbouncerFailoverConfig='pgbouncer_failover_config';
my $pgbouncerStandardConfig='pgbouncer_standard_config';
my $fenceLockDbs='fence_lock_dbs';
my $fenceLockCommand='fence_lock_command';
my $fenceMoveCommand='fence_move_command';
my $fenceUnlockCommand='fence_unlock_command';
my $statusTable='status_table';
my $dbport='dbport';
my $table='table';
my $schema='schema';
my $sleep='sleep';
my $log='logFile';
my $pidFile='pidFile';
my $costDelay='cost_delay';
my $analyze='analyze';
my $stopTime="stop_time";
my $startTime="start_time";
my $instanceName="instanceName";
my $pgProbe="pgProbe";
my $hostPing="hostPing";
my $escalation="escalation";
my $defcon3Frequency="defcon3_probe_frequency";
my $defcon2Retries="defcon2_retries";
my $defcon2RetrySleep="defcon2_probe_frequency";
my $triggerFile="triggerFile";
my $logConfig="logConfig";

# Config Sections
my $globalSection="global";
my $masterSection="master";
my $appHost1="app01";
my $appHost2="app02";
my $appHost3="app03";
my $failoverSection="failover";
my $slaveSection="slave";

#Constants

# Parse the config file options
%Config=ReadConfig($confFile);

# Slave DB should be using same auth as master
$Config{$slaveSection}{$dbname}=$Config{$masterSection}{$dbname};
$Config{$slaveSection}{$dbuser}=$Config{$masterSection}{$dbuser};
$Config{$slaveSection}{$dbpass}=$Config{$masterSection}{$dbpass};

# Global config values
my @FailoverCommands;

# Build the trigger file command
# Note: This is an area of contention, without testing that the
#       trigger file is writeable, we have no way of being sure that
#       failover will succeed when the moment comes.  
#       The admin needs to be sure that this is really ready to go!!
my $triggerCommand="touch " . $Config{$globalSection}{$triggerFile};

# Error, certain options required in config
die "No logging config defined for in $confFile\n" if (!defined($Config{$globalSection}{$logConfig}));
die "No pid file defined for in $confFile\n" if (!defined($Config{$globalSection}{$pidFile}));

# Prep our failover commands for execution
# Note: This is an area of contention, without testing that the
#       commands are valid and executable, we have no way of being sure that
#       failover will succeed when the moment comes.  
#       The admin needs to be sure that these are really ready to go!!

foreach my $options ( keys %{ $Config{$failoverSection} } )
{
    push(@FailoverCommands, $Config{$failoverSection}{$options});
}

# Fire up log4perl
Log::Log4perl::init_and_watch($Config{$globalSection}{$logConfig},'HUP');
my $logger = Log::Log4perl->get_logger();

# Information for normal startup
# We do not print this when nagiosStatus is requested
if( ! $nagiosStatusCheck )
{
    print "\n Welcome to $programName, an auto-failover system for PostgreSQL streaming replication\n";
    print "\tInstance Name: " . $Config{$globalSection}{$instanceName} . "\n";
    print "\tConfig: ". $confFile . "\n"; 
    print "\tPidFile: " . $Config{$globalSection}{$pidFile} . "\n";
}

if ( ! $executeImmediateFailover && !$endWorker && !$nagiosStatusCheck ) 
{
    print "\tFailover Command: " . $triggerCommand . "\n";
    print "\t\tThe Failover command must be able to run successfully.\n"
         ."\t\tIf it cannot, the slave database will NOT be promoted.\n";
}

# If the user wants to see the config options after parsing
if ($showConfig)
{
    print "\n  $programName Options (post-parse)\n";
    foreach my $section ( keys %Config)
    {
        print "Section: $section\n";
        foreach my $param ( keys %{ $Config{$section} } ) 
        {
            print "\tOption: $param, Value: ". $Config{$section}{$param} ."\n";
        }
    }
}

# Define the worker process
my $worker = Proc::Daemon->new(
        work_dir=> dirname($0),
        pid_file=> $Config{$globalSection}{$pidFile}
    );

my $np = Nagios::Plugin->new(
            usage => "Checks the worker process status");
$np->getopts;

# Just check if the worker is running or not
if ( $workerStatusCheck )
{
    if ( $worker->Status() ) { 
        print "\n\tWorker: is UP\n";
    }
    else {
        print "\n\tWorker: is DOWN\n";
    }
        
    print "\n";
    exit(0);
}

# Nagios output of a status check 
# Currently, only supported on unix

my $DS=0;
if ( $nagiosStatusCheck )
{
	if ( $worker->Status() )
	{
		# Get the PID and check on the command string of pgHA
		my $fpid = $worker->Status();
		#printf "PID is:".$fpid ."\n";
		$DS = `ps -c $fpid | tail -1 | awk '{print $8}'`;
		    	
		my @str = split('-',$DS);
		if ( $str[1] eq "Defcon3" )
		{
			$np->nagios_exit( OK, "Worker process UP ** status is :: $str[1]");
		}
		elsif ( $str[1] eq "Defcon2" )
		{
			$np->nagios_exit( WARNING, "Worker process is UP *** ESCALATED TO :: $str[1]");
		}
		elsif ( $str[1] eq "Defcon1" )
		{
			$np->nagios_exit( CRITICAL, " Worker process is UP *** ESCALATED TO :: $str[1]");
		}
	}
	else
	{
		$np->nagios_exit ( CRITICAL, "Worker: is Down");
	}
	
	print "\n";
	exit(0);
}

if ( $executeImmediateFailover == 1 ) 
{
    print "\n\t WARNING: Executing immediate failover. Are you sure? [y/N] ";
    my $response=<STDIN>;
    chomp ($response);
    if ( $response eq 'n' || $response eq 'N' ) 
    {
        print "You have chosen not to execute immediate failover.  Exiting...\n";
        exit (1);
    }
    elsif ( $response eq 'y' || $response eq 'Y' ) 
    {
        print "You are now going to execute immediate failover in 5 seconds (CTRL+C to exit!)\n";
        for ( my $i=0; $i<5;$i++)
        {
            print "Failover in : " . (5-$i) . "\n";
            sleep 1;
        }
        EndWorker($worker,$Config);
        my %State;
        Defcon1(\%State,\%Config);
        exit(2);
    }
    else
    {
        print "You have entered an invalid keystroke, Exiting...\n";
        exit (1);
    }
}
if ( $pushButton ) 
{
    print "\tEnabling failoverd in push-button only mode.\n";
    print "\tAutomated failover will be disabled\n";
    if ( $endWorker || $workerStatusCheck 
        || $startWorker || $executeImmediateFailover )
    {
        print "Push-Button mode cannot be enabled with other ACTIONS\n";
        exit(1);
    }
    else
    {
        $dryRun=1;
        $startWorker=1;
    }
}
# Stop the worker from running
if ( $endWorker ) 
{
    EndWorker($worker,$Config);
    if ( UnSetSpring(%Config,$logger) != 1 )
    {
        $logger->info("Cannot unset failover configuration!");
        $logger->info("You will need to manually reset pgbouncer to the orignal config");
        print "Cannot unset failover configuration!\n";
        print "You will need to manually reset pgbouncer to the orignal config\n";
    }
}
# Start the failoverd monitoring process
elsif ( $startWorker )
{
    print "\n\tInitializing... \n";

    # If the worker is already running, just print a message and exit
    # This is a very key piece of functionality, we need a watchdog-style
    # cron job to be able to attempt a daemon start in the event that this 
    # daemon has died for some reason.  
    if ( $worker->Status() ) 
    {
        print "FAILED!\n\n";
        print "Worker is up and running already, exiting!\n";
        exit (1);
    }

    # Before starting, make sure that all of our system components are up and running
    # If they aren't, this could lead to attempting to perform a promotion of the slave 
    # accidentally based on the start-up order of the cluster.
    
    # NB: This can also protect us from the case where we perform a failover 
    #   and the cron watchdog attempts to restart the daemon.  
    if ( InitProbes(%Config,$logger) != 1)  
    {
        print "The system is not 100% online.  Canceling ".$programName." startup\n";
        exit (1);
    }

    my %SlaveDelay=CalculateSlaveDelay($Config);
    #print "Delay: " . $SlaveDelay{'time_delay'} . "\n";
    my $tableCheck= "INSERT INTO " . $Config{$masterSection}{$statusTable} 
                   . " (dttm,event,properties) VALUES (now(),'".$programName."Startup',"
                   . "'DelayTime:".$SlaveDelay{'time_delay'} . ",DelayKB:".$SlaveDelay{'replay_delay'}."')";

    $connStr = "port=".$Config{$masterSection}{$dbport}
              ." host=".$Config{$masterSection}{$dbhost}
             ." dbname=".$Config{$masterSection}{$dbname};
    my @query=($tableCheck);

    my $statusTableOkay=ExecuteQueries($connStr,$Config{$masterSection}{$dbuser},
                                        $Config{$masterSection}{$dbpass},@query);
                                        
    if ( $statusTableOkay != 1 ) 
    {
        print " Status Table not created, please fix this\n";
        exit(1);
    }

    $logger->info("==Startup==: Verifying application host config");
    print "\t === Verifying application host config === \n";
    if ( VerifyPgBouncerConfig(%Config,$logger) != 1 ) 
    {
        $logger->info("Application hosts and cluster do not match");
        print "\tApplication hosts and cluster have different master\n"
              ."\tdatabase IP addresses.  Please correct the configuration\n\n";
        print "Exiting.\n";
        exit(1);
    }
    else
    {
        $logger->info("==Startup==: Application host config verified");
        print "\t === Application host config verified=== \n";
    }

    # Once we have verified that the system is online, we need
    # to set the failover 'spring'.  
    $logger->info("==Startup==: Arming Failover mechanism");
    print "\t === Arming Failover mechanism === \n";
#    if ( SetSpring(%Config,$logger) != 1 )
    if ( 0 )
    {
        $logger->info("Cannot set failover configuration, exiting...");
        print "Cannot set failover configuration, exiting...\n";
        exit(1);
    }
    else 
    {
        $logger->info("Your cluster is now ready to failover.");
        print "\n\n\tYour cluster is now ready to failover.\n";
        print "\trestarting pgBouncer without taking this into\n";
        print "\tconsideration could cause unexpected downtime\n\n";
        print "\t === Failover mechanism armed === \n";
    }

    # Initialize the worker thread
    my $workerPid = $worker->Init;

    # If my pid = 0, I am the worker, go into my function, 
    # otherwise, skip the next line and exit the main program
    Defcon3(%Config) unless ( $workerPid );

    if ( $worker->Status() ) 
    {
        print "SUCCESS!\n\n";
        printf "Worker active on PID: ".$worker->Status() ."\n";
    }
    else
    {
        print "FAILED\n";
        print "See log:\n";
        print "\t" . $Config{$globalSection}{$log} . "\n";
        exit (1);
    }
}
exit (0);

############# End Main Program ###############

# Sub: VerifyConfig
# Inputs:
#       %configHash - Hashmap representing parsed configuration file
#       
# Single Return: @MissingItems
#       Array containing a list of missing configuration entries

sub VerifyConfig { 

    my ( %configHash ) = @_;
    my @MissingItems;
    my %requiredOptions;
    
    # If you add something to the config file that is required for startup, add it here.
    # Each section gets a key in the $requiredOptions hash.  Then, just make an array of the
    # options to search for
    $requiredOptions{$globalSection} = ["pidFile",$logConfig,$instanceName,$defcon2Retries,$defcon2RetrySleep,$triggerFile];
    $requiredOptions{$masterSection} = ["dbname","dbport","dbhost",$statusTable];
    $requiredOptions{$slaveSection}  = ["dbport","dbhost"];
#    $requiredOptions{$failoverSection} = ["failoverCommand1","failoverCommand2"];

    foreach my $key ( keys %requiredOptions)
    {
        #print "Checking section: $Gkey\n" ;
        foreach my $val ( @{$requiredOptions{$key}} ) 
        {
         #   print "\tChecking for : " . $val . " In configHash\n";
         #   print "\tFound: " . $configHash{$key}{$val} . "\n";
            if ( ! defined ( $configHash{$key}{$val} ) )
            { 
         #       print "\t\tIt's not there!!!\n";
                push (@MissingItems,"$key | $val");
            }
        }
    }
    return @MissingItems;
}

# Sub: UnSetSpring
# Inputs:
#       %configHash  - Hash of configuration options
#
# Single Return: $statusCode  
#       A boolean declaring that the system succesfully unset
#       all of the appropriate configs for failover
#
# Purpose:
#       This function is designed to 'unset the failover spring'
#       this will ensure that we can restart pgbouncer without 
#       accidentally causing a failover
sub UnSetSpring {
    my (%configHash, $logger) = @_;

    # Set pg_hba on master to block everyone except for cluster manager
    # my $springMaster="ssh " . $configHash{$masterSection}{$dbhost}

    # Foreach apphost, set pgbouncer.ini to original config
    my $statusCode=0;
    foreach my $key ( keys %configHash ) 
    {
        if ( $key =~ m/^app/ )
        {
            my $setSpringCmd="ssh " . $configHash{$key}{$dbhostUser} . "@" . $configHash{$key}{$dbhost}
                            . " \"cp $configHash{$key}{$pgbouncerStandardConfig} $configHash{$key}{$pgbouncerConfig}\"";

            print "\tUn-Setting failover config for pgBouncer on: $key... ";
            
            my $thisStatus=system ($setSpringCmd);
            print "DONE!\n" if ( $thisStatus == 0);
            print "FAILED\n" if ( $thisStatus == 1);
            $statusCode=$statusCode || $thisStatus 
        }
    }
    return !$statusCode;
}

# Sub: SetSpring
# Inputs:
#       %configHash  - Hash of configuration options
#
# Single Return: $statusCode  
#       A boolean declaring that the system succesfully set
#       all of the appropriate configs for failover
#
# Purpose:
#       This function is designed to 'set the failover spring'
#       this will ensure that we can enact a failover without
#       having to ssh to the server during the critical event.
sub SetSpring {
    my (%configHash, $logger) = @_;

    # Set pg_hba on master to block everyone except for cluster manager
    # This is the best way to fence the system, BUT is currently disabled for now
    # my $springMaster="ssh " . $configHash{$masterSection}{$dbhost}

    # Foreach apphost, set pgbouncer.ini to failover config
    my $statusCode=0;
    foreach my $key ( keys %configHash ) 
    {
        if ( $key =~ m/^app/ )
        {
            my $setSpringCmd="ssh " . $configHash{$key}{$dbhostUser} . "@" . $configHash{$key}{$dbhost}
                            . " \"cp $configHash{$key}{$pgbouncerConfig} $configHash{$key}{$pgbouncerConfig}_old"
                            . " && cp $configHash{$key}{$pgbouncerFailoverConfig} $configHash{$key}{$pgbouncerConfig}\"";

            print "\tSetting failover config for pgBouncer on: $key... ";
            
            my $thisStatus=system ($setSpringCmd);
            print "DONE!\n" if ( $thisStatus == 0);
            print "FAILED\n" if ( $thisStatus == 1);
            $statusCode=$statusCode || $thisStatus 
        }
    }
    return !$statusCode;
}

sub VerifyPgBouncerConfig {

    my (%configHash,$logger)=@_;

    my $safeToStart=1;

    foreach my $key ( keys %configHash )
    {
        if ( $key =~ m/^app/ )
        {
            my $connStr="port=".$Config{$key}{$dbport}
                       ." host=".$Config{$key}{$dbhost}
                       ." dbname=".$Config{$key}{$pgbouncerDb};

            my $dbh=DBI->connect("dbi:Pg:".$connStr,
                                $Config{$key}{$pgbouncerDbUser},
                                $Config{$key}{$dbpass} );

           my $sth=$dbh->prepare('show databases');

           print "\t\tFor host: $key\n";
           $sth->execute();
           while ( $row=$sth->fetchrow_hashref())
           {
                if ( $Config{$key}{$fenceLockDbs} =~ m/$row->{'name'}/ ) 
                {
                    if ( $row->{'host'} ne $Config{$masterSection}{$dbhost})
                    {
                        print "\tDb              : " . $row->{'name'} . "\n";
                        print "\tPoints to       : " . $row->{'host'} . "\n";
                        print "\tCurrent Master  : " . $Config{$masterSection}{$dbhost} . "\n\n";
                        $safeToStart=0;
                    }
                }
           }
           $sth->finish();
           $dbh->disconnect();
        }
    }
    return $safeToStart;
}

sub CalculateSlaveDelay
{
    my ( $Config ) = @_;

    my %MasterSlaveOffset;
    $MasterSlaveOffset{'masterOffset'}{'query'}="SELECT pg_current_xlog_location()";
    $MasterSlaveOffset{'slaveReceived'}{'query'}="SELECT pg_last_xlog_receive_location()";
    $MasterSlaveOffset{'slaveReplayed'}{'query'}="SELECT pg_last_xlog_replay_location()";
    $MasterSlaveOffset{'slaveTimeDelay'}{'query'}="SELECT CASE when pg_last_xlog_receive_location() = pg_last_xlog_replay_location() THEN 0 "
                                                 ."ELSE date_part('seconds',now()-pg_last_xact_replay_timestamp()) END AS log_delay";
    foreach my $key ( keys %MasterSlaveOffset ) 
    {

        my $connStr;
        my $user;
        my $pass;
        if ( $key =~ m/^master/ ) 
        {
            $connStr = "port=".$Config{$masterSection}{$dbport}
                      ." host=".$Config{$masterSection}{$dbhost}
                     ." dbname=".$Config{$masterSection}{$dbname};
            $user=$Config{$masterSection}{$dbuser};
            $pass=$Config{$masterSection}{$dbpass};
        }
        elsif ( $key =~ m/^slave/ ) 
        {
            $connStr = "port=".$Config{$slaveSection}{$dbport}
                      ." host=".$Config{$slaveSection}{$dbhost}
                     ." dbname=".$Config{$slaveSection}{$dbname};
            $user=$Config{$slaveSection}{$dbuser};
            $pass=$Config{$slaveSection}{$dbpass};
        }

        $connStr="dbi:Pg:connect_timeout=1 ".$connStr;
        $logger->debug("Connecting: $connStr");
        my $DB=DBI->connect($connStr,$user,$pass); 
        $logger->debug("Complete Connect");
       
        my $sth=$DB->prepare($MasterSlaveOffset{$key}{'query'});

        $logger->debug("Executing");
        my $rv=$sth->execute();
        $logger->debug("Complete");
        if ( ! $rv ) 
        {
            $logger->error("Could not execute query: ". $MasterSlaveOffset{$key}{'query'} . " on $connStr\n");
        }
        else
        {
            if ( $sth->rows != 1 ) 
            {
                $logger->debug($MasterSlaveOffset{$key}{'query'} . " returns NOT 1 row");
            }
            $MasterSlaveOffset{$key}{'result'}=(($sth->fetchrow_array())[0]);
            if ( $MasterSlaveOffset{$key}{'result'} eq '' )
            {
                $MasterSlaveOffset{$key}{'result'}="0/0";
            }
        }
        $sth->finish;
        $DB->disconnect();
    }
   
    my %Delay;
    $Delay{'master_num'}    = CalculateNumericalOffset($MasterSlaveOffset{'masterOffset'}{'result'});
    $Delay{'receive_delay'} = ($Delay{'master_num'} - CalculateNumericalOffset($MasterSlaveOffset{'slaveReceived'}{'result'}));
    $Delay{'replay_delay'}  = ($Delay{'master_num'} - CalculateNumericalOffset($MasterSlaveOffset{'slaveReplayed'}{'result'}));
    $Delay{'replay_delay'}  = $Delay{'replay_delay'} / 1024;
    $Delay{'time_delay'}    = $MasterSlaveOffset{'slaveTimeDelay'}{'result'};

    return %Delay;

}
# Sub: CalculateNumericalOffset
# Inputs: 
# Outputs:
# Purpose:
#
#

sub CalculateNumericalOffset
{
    my $stringofs = shift;

    my @pieces = split /\//, $stringofs;
    die "Invalid offset: $stringofs" unless ($#pieces == 1); 

    # First part is logid, second part is record offset
    return (hex("ffffffff") * hex($pieces[0])) + hex($pieces[1]);
}

# Sub: InitProbes
# Inputs:
#       %configHash  - Hash of configuration options
#
# Single Return: safeToStart  
#       A boolean declaring if all resources are currently available and 
#       it is safe to enter H/A auto-failover mode
#
# Purpose:
#       This subroutine will ensure that all services are showing green 
#       before allowing startup.  If all services are not green, 
#       the system may (incorrectly) attempt to promote the standby 
#       database
sub InitProbes { 
    my ( %configHash,$logger ) = @_;

    my $safeToStart=1;
    my %EachProbe;

    # Check that the PgProbe is working for each of the configured servers.  
    # If it is not, then, we need to return a 'false' (i.e. system not
    # green ).

    foreach my $key ( keys %configHash ) 
    {
        # We need to check that all of the defined databases
        #  are up and running
        if ( $key =~ m/^master/ || $key =~ m/^app/ || $key =~ m/$slaveSection/ ) 
           
        {
            $EachProbe{$key}=0;
            print "\tNow Checking: " . $key . "\n\t";

            my $connStr="port=".$Config{$key}{$dbport}
                          . " host=".$Config{$key}{$dbhost}
                          ." dbname=".$Config{$key}{$dbname};

            $EachProbe{$key}=CheckPgDatabase($connStr, 
                                             $Config{$key}{$dbuser}, 
                                             $Config{$key}{$dbpass});
            print "\t\t" . $key . " : ";
            if ( $EachProbe{$key} == 1 )
            {
                print "OK\n";
            }
            else
            {
                print "FAILED\n";
            }
            $safeToStart = $safeToStart && $EachProbe{$key};
        } 
    }

    return $safeToStart;
}

# Sub: ReadConfig
# Inputs: 
#       $confFile - String containing path to config file
#       $logger   - Handle to the log writer
#
# Single Return: %ConfigHash
#       A hashmap containing the parsed values of the config file
#
# Purpose:
#       This subroutine will load and return a hashmap filled with 
#       key:value pairs from the config file.
#
sub ReadConfig {

    my ($confFile,$logger) = @_;

    my %configHash;

    # The Config::IniFiles module let's me directly tie a config file to a hash. 
    tie %configHash , 'Config::IniFiles',(-file => $confFile ) ;

    if ( ! %configHash ) 
    {
        print "Cannot parse configuration file, errors below:\n";
        print @Config::IniFiles::errors;
        exit (1); 
    }

    # Make sure all of required options are present
    my @MissingConfig = &VerifyConfig(%configHash);

    if ( @MissingConfig ) 
    { 
        print "You have some missing config options\n";
        print "====================================\n";
        print "|    Section     |    Option       |\n";
        print "====================================\n";
        foreach my $missing ( @MissingConfig ) 
        {
            print "\t$missing\n";
        }
        print "====================================\n";
        exit (1);
    }
    else # Good Config
    {
        # If our config file passes syntax and logical checks
        # Try to open the log4perl config file.  If we can't, don't try to start 
        open LOGCONFIG, $configHash{$globalSection}{$logConfig} or die ("Cannot open logging config file\n");
    
        while ( <LOGCONFIG> ) 
        {
            if ( m/log4perl.appender.LOG.filename/ ) 
            {
                $configHash{$globalSection}{$log}=(split(/=/))[1];
                chomp $configHash{$globalSection}{$log};
            }
        } 
        close (LOGCONFIG);
    }
    return %configHash;
}

sub EndWorker {

    my ($worker,$Config) = @_;
    print "\n\tAttempting to end $programName-Worker: \n";
    print "\t\tWorker already down... " if ( ! $worker->Status());

    # Keep printing until the worker actually dies
    while ( $worker->Status() )
    {
        $logger->info("Sending kill signal to ". $worker->Status() );
        print "\t\tSending kill signal to ". $worker->Status();
        $worker->Kill_Daemon();
        sleep 2;
    }

    # The Proc::Daemon library should take care of this for us, this is just an extra measure
    unlink($Config{$globalSection}{$pidFile}) if (! $worker->Status());

    print " Complete!\n\n";
}

#
# Sub: AnalyzeState
# Inputs:
#       %State - Hash of Hashes, pointing to the system state
# Outputs:
#       
# Purpose:
#       This subroutine determines whether or not we should move
#       to a heightened alert system

# Logic Doc:
#       The logic for determining system failover can be broken down into a
#       fairly simple logic table: 
#       

sub AnalyzeState { 
    
    # State hash of hashes is passed to us by the main loop
    my (%State)=@_;

    # Return variable, denotes what type of escalation is warranted
    my $escalation=0;

    my $pgState=0;
    my $pingState=0;

    foreach my $key ( keys %State ) 
    { 
        if ( $key =~ m/^app/  || $key =~m/^master/ ) 
        {
            $logger->debug("$key: $State{$key}{$pgProbe}");
            $logger->info("Postgres On: $key = $State{$key}{$pgProbe}");
            $logger->info("Ping On    : $key = $State{$key}{$hostPing}");

            # Because we're only down if everyone is down, 
            # a logical OR (||) works here
            $pingState=$pingState || $State{$key}{$hostPing};
            $pgState = $pgState || $State{$key}{$pgProbe};
        }
    }

    if ( $pgState == 0 ) 
    {
        # Postgres is showing down across the board
        if ( $pingState == 0 ) 
        {
            # All hosts are also down
            # this means that we are most likely isolated
            # We should fence ourselves from the cluster 
            $escalation=2;
        }
        elsif ( $pingState == 1 ) 
        {
            # At least one host is pingable which means we are 
            # not isolated from the cluster.  We should take 
            # ownership of the DB, i.e. failover to slave
            $escalation=1;
        }
    }
    elsif ( $pgState == 1 ) 
    {
        # All pgProbes are showing UP, this means that the 
        # master DB is healthy and there is no reason to shutdown
        $escalation=0
    }

    return $escalation;
}

sub ExecuteQueries
{
    my ( $connStr, $user, $pass, @queries, $timeout ) = @_;

    my $return=0;
    $logger->debug("Preparing to execute query");
    # Default the query timeout to 10 seconds
    $timeout=10 if ( !$timeout );
   
    $logger->debug("NumQueries: ".@queries);

    # No matter what the connection String is, always add a 1 second timeout
    $connStr="dbi:Pg:connect_timeout=1 ".$connStr;
    $logger->debug("Connecting to: $connStr");
    my $DB=DBI->connect($connStr,$user,$pass); 

    if ( $DB )
    {
        $logger->debug("Connect Complete");
        
        foreach my $query ( @queries ) 
        {
            $logger->debug("GOING TO RUN: $query"); 
            my $sth=$DB->prepare($query);
            my $rv=$sth->execute();
            $logger->error($DB->errstr) if ( $DB->err ) ;
            if ( ! $rv ) 
            {
                $logger->error("Could not execute query: $query on $connStr");
                $ret=0;
            }
            else
            {
                $sth->finish();
                $ret=1;
            }
        }
        $logger->debug("End of Execute");
        $DB->disconnect();
    }
    else
    {
        $logger->error("Cannot connect to DB: $connStr");
        $logger->error("Not Executing Queries");
    }
    return $ret;
}

#
# Sub: CheckPgDatabase
# Inputs:
#       connStr - postgres standard connection string
#       user        - postgres db username
#       pass        - postgres db password
# Output:
#       isDBUp      - (Boolean) 1 if database is up, 0 if it is down
sub CheckPgDatabase
{
        my ( $connStr, $user, $pass,$query ) = @_;
        my $isDBUp=1;
        my $pgTimeout=4;    # In 1/2 second increments

        $logger->debug("Attempting connect: ".$connStr);
            
        # No matter what the connection String is, always add a 1 second timeout
        $connStr="dbi:Pg:connect_timeout=1 ".$connStr;

        my $DB=DBI->connect($connStr,$user,$pass); 

        if ( $DB ) 
        {
            $logger->debug("Complete connect");
            
            if ( !$query ) { 
                $query="SELECT 2+2";
                $logger->debug("Query to execute: ".$query);
            } 
            
            # Execute the query directly
            #   This is found to have issues if there is a 'strange' or 
            #   otherwise inexplicable hang when we do something
            #   This was deprecated in favor of async query execution which 
            #   lets us enforce our own timeout on the query without 
            #   needing to change a server-side setting
#            my $sth=$DB->prepare($query);
#            my $rv = $sth->execute();
            
            # Kick off query            
            my $queryComplete=0;
            my $execTimer=0;

            $logger->debug("Dispatching query");
            $DB->do($query,{pg_async => PG_ASYNC});

            # Native way of performing < 1 second sleep
            # This is to give the pg_async a chance to 
            # complete before getting into the actual
            # timeout loop below.
            # NB:
            #  Although the HiRes::Timer package is used for
            #  this, this package is native in all versions of
            #  perl, no extra modules required
            select ( undef, undef, undef, .500);

            if ( $DB->pg_ready()  && $DB->pg_result() && !$DB->err)
            {
                $queryComplete=1;
                $logger->debug ("Query Complete: ".$execTimer ); 
            }
            
            # Query timeout handling.  
            # Don't let the query run for more than 2 seconds without canceling.
            while ( $execTimer < $pgTimeout && $queryComplete!=1)
            {
                if ( $DB->pg_ready() && !$DB->err)
                {
                    $logger->debug("Query Complete: ".$execTimer ); 
                    if ( $DB->pg_result() ) 
                    {
                        $queryComplete=1; 
                    }
                    else
                    {
                        $queryComplete=0;
                    }
                }
                else
                {
                    $logger->debug("$sleep $execTimer");
                    # High speed sleep (1/2 second)
                    select (undef,undef,undef,.500);
                }
                $execTimer++;
            }

            if ( $queryComplete != 1 ) 
            {
                $isDBUp = 0 ;
                $logger->info("DB Check Failed");
            }
            $DB->disconnect();
        }
        else
        {
            $logger->info("Failed connect");
            $isDBUp=0;
        }

        return $isDBUp;

} # sub CheckPgDatabase

#
# sub: PingHost
# Inputs: 
#       $host     - The host to ping (should be an IP, but not required)
#       $protocol - Which protocol to use, defaults to icmp 
#       $timeout  - The timeout to use in seconds, defaults to 1
# Outputs:
#       0  - If the ping was not successful
#       1  - If the ping was successful
# Purpose:
#       This function is designed to ping a host to see if it is
#       responding on the network 
sub PingHost { 
    my ($host, $timeout, $protocol)=@_;
    
    $protocol='icmp' if ( !$protocol );
    $timeout=1 if ( !$timeout );

    # my $pinger=Net::Ping->new($protocol,$timeout);
    my $pinger=Net::Ping->new();
   
    # If the ping is successful within the timeout, 
    # return a 1, else return a 0
    return ( $pinger->ping($host,$timeout) ? 1:0 );

} # sub PingHost

#
# Sub: Defcon3
# Inputs:
#       %Config - Hashmap of the loaded configuration file
#
# Outputs: None
#
# Purpose:
#       This is the actual background daemon code run 
#       by $programName ($programName-Defcon3) after initialization.  
#
sub Defcon3 {
   
    # Config hash is passed to us by the 'forker'
    my (%Config)=@_; 

    # Create the name of this instance and set process title
    my $workerName=$Config{$globalSection}{$instanceName};
    $0 = "$programName-Defcon3-OK-" . $workerName;

    # Setup a 'State' data-structure.  This will hold the state of
    # each probe for each run.  This is what Defcon 2 will be analyzing
    my %State;

    # Get the logger hooked up to this process
    Log::Log4perl::init_and_watch($Config{$globalSection}{$logConfig},'HUP');
    my $logger = Log::Log4perl->get_logger();
    
    $logger->info("========Initializing $programName ==============");
    $logger->info($Config{$globalSection}{$log});

#    $SIG{'USR1'} = sub { $logger->warning("Caught SIGUSR1: reloading Log4Perl"); };

    $logger->info ("Initializing $programName-Worker:".$workerName);

    # Close and re-open stderr to a worker-specific log file
    #   (defined in the configuration)
    close STDERR;
    open STDERR,'>',$Config{$globalSection}{$log};

    my $probeFrequency = $Config{$globalSection}{$defcon3Frequency} || 1;

    # The main 'forever' loop of the $programName-Worker
    while (1)
    {
        $logger->debug("Top of worker loop");
        $logger->info("=== Attention: System is in DRY-RUN mode.  No automated failover will occur") if ($dryRun == 1);

        # If the stderr log file moved or is being rotated, 
        # let go of the handle and then re-open it to 
        # re-create the file (allows log rotation)
        if (! -e $Config{$globalSection}{$log} )
        {
            close STDERR; 
            open STDERR,'>',$Config{$globalSection}{$log};
        }

        # Pause between checks
        sleep($probeFrequency);

        # Check the master DB
        my $masterStr="port=".$Config{$masterSection}{$dbport}
                      . " host=".$Config{$masterSection}{$dbhost}
                      ." dbname=".$Config{$masterSection}{$dbname};

        $State{$masterSection}{$pgProbe}=CheckPgDatabase($masterStr, 
                                                         $Config{$masterSection}{$dbuser}, 
                                                         $Config{$masterSection}{$dbpass});
        
        $State{$masterSection}{$hostPing} = $State{$masterSection}{$pgProbe};
        $logger->debug("Going to analyze state"); 

        my $shouldEscalate=AnalyzeState(%State);

        $logger->debug("Analyze returns: " . $shouldEscalate . "\n");

        if ( $shouldEscalate == 1 || $shouldEscalate==2 ) 
        {
            
            $logger->info("Master DB not responding, escalating");
            Defcon2(\%State,\%Config);
        }
        else
        {
            $logger->info("Master DB OK");
        }

        $logger->debug("Completed worker Run\n");

    } # End: while (1): Main loop

    exit (0);

} # End: sub Defcon3

#
# Sub: Defcon2
# Inputs:
#       %$State  - Reference to system state hashmap 
#       %$Config - Reference to hashmap of config options
#
# Outputs: None
#
# Purpose:
#       This is the actual background daemon code run 
#       by $programName ($programName-Defcon2) after initialization.  

sub Defcon2
{
    $logger->info("System has escalated to Defcon2");  

    # Get the State and Config Hashes passed to us
    # These MUST be pass by reference here.  
    my ($StateP, $ConfigP) = @_;
    
    # Convert refs to hash
    my %State = %$StateP;
    my %Config = %$ConfigP; 

    # Create the name of this instance and set process title
    my $workerName=$Config{$globalSection}{$instanceName};
    $0 = "$programName-Defcon2-WARNING-" . $workerName;
    
    my $defcon2Counter=$Config{$globalSection}{$defcon2Retries};
    my $defcon2ProbeSleep=$Config{$globalSection}{$defcon2RetrySleep};
    
    my $goToDefcon1=0;
    my $stcnith=0;

    while ( $defcon2Counter > 0  ) #&& $goToDefcon1 != 1 ) 
    {
        $logger->info("Remaining attempts: " . $defcon2Counter );
        $logger->debug("defcon2 dropdown: $defcon2Counter");
        $defcon2Counter -= 1;

        # Execute probe on all systems
        foreach my $key ( keys %Config ) 
        { 
            if ( $key =~ m/^app/ || $key =~ m/master/) 
            {
                $logger->info("Checking: ".$key);

                my $connStr="port=".$Config{$key}{$dbport}
                           ." host=".$Config{$key}{$dbhost}
                           ." dbname=".$Config{$key}{$dbname};

                $State{$key}{$pgProbe}=CheckPgDatabase($connStr,
                                        $Config{$key}{$dbuser},
                                        $Config{$key}{$dbpass});

                # Prime the ping Check with the pgProbe
                $State{$key}{$hostPing} = $State{$key}{$pgProbe};
                
                if ( $State{$key}{$pgProbe} != 1 ) 
                {
                    $logger->info("$key is registering DOWN, checking ping...");
                    $State{$key}{$hostPing}=PingHost($Config{$key}{$dbhost});

                    if ( $State{$key}{$hostPing}==1 )
                    {
                        $logger->info("$key ping is UP"); 
                    }
                    else
                    {
                        $logger->info("$key ping is DOWN");
                    }
                }   

            } # if ( this is an apphost or the master DB )
        } #foreach host in the config)

        $logger->debug("Going to analyze state"); 

        my $shouldEscalate=AnalyzeState(%State);

        $logger->debug("Analyze returns: " . $shouldEscalate . "\n");

        # Where do we go based on the analysis?
        if ( $shouldEscalate == 1) 
        {
            # The system has determined that we need to escalate
            $goToDefcon1=1;
            $stcnith=0;
            my $slaveStr="port=".$Config{$slaveSection}{$dbport}
                        ." host=".$Config{$slaveSection}{$dbhost}
                        ." dbname=".$Config{$slaveSection}{$dbname};
            
            my $isConnected=CheckPgDatabase($slaveStr, 
                                            $Config{$slaveSection}{$dbuser}, 
                                            $Config{$slaveSection}{$dbpass});
            if ( $isConnected == 1 ) 
            {
                $logger->info("Slave is Available for failover");
            }
            else 
            {
                $logger->info("Slave is registering as DOWN!  No failover path is available.");
                $goToDefcon1=0;
            }
        }
        elsif ( $shouldEscalate == 2 ) 
        {
            # The system is determined that we have been cut 
            # off from the cluster. Here, we need to set 
            # STCNITH (Shoot the Current Node in the Head) 
            # and make sure that we don't try to 
            # promote ourself when we're the one who is dead
            $stcnith=1; 
        }
        elsif ( $shouldEscalate==0 && $goToDefcon1==1 ) 
        {
            # The system has determined that we no longer need to escalate
            $logger->info("===== Service recovering ======");
            $defcon2Counter=$Config{$globalSection}{$defcon2Retries};
            $goToDefcon1=0;
            $stcnith=0;
        }

        if ( $defcon2ProbeSleep ) 
        {
            $logger->debug("Sleep between probes: " . $defcon2ProbeSleep );
            sleep $defcon2ProbeSleep;
        }
    }#while ( $defcon2Counter > 0  )

    if ( $stcnith == 1 ) 
    {
        SelfFence(\%State,\%Config);    
    }
    if ( $goToDefcon1 == 1 ) 
    { 
        Defcon1(\%State,\%Config);
    }

    # This code will only be executed if we run through all of Defcon2 and restore to Defcon3
    $logger->info("Defcon2 Complete, Master DB is accessible, continuing") if ( $statsMode != 1 );
    $logger->info("Defcon2 Complete, stats mode is active, reverting to Defcon3") if ( $statsMode == 1 );
    $State{$masterSection}{$pgProbe}=1;
    $State{$masterSection}{$escalation}=0; 

} # End: sub Defcon2

# Sub: SelfFence
# Inputs:
#       %$State  - Reference to system state hashmap 
#       %$Config - Reference to hashmap of config options
#
# Outputs: None
#   
# Purpose:
#       This function is called when failoverd has determined that it 
#       has become an isolated liability in the cluster
#       Instead of promoting the slave, I am going to shutdown
#       to avoid a false-positive failover
sub SelfFence
{
    $logger->info("==== System has escalated to SelfFence ====");
    $logger->info("This slave has been deemed isolated from the...");
    $logger->info("remainder of the cluster and will disable $programName...");
    $logger->info("to avoid false-failover"); 

    if ( $statsMode != 1) 
    {
        exit(1);
    }
}

#
# Sub: Defcon1
# Inputs:
#       %$State  - Reference to system state hashmap 
#       %$Config - Reference to hashmap of config options
#
# Outputs: None
#
# Purpose:
#       This is the actual background daemon code run 
#       by $programName ($programName-Defcon1) after initialization.  
sub Defcon1
{
    # Get the State and Config Hashes passed to us
    # These MUST be pass by reference here.  
    my ($StateP, $ConfigP) = @_;
    
    # Convert refs to hash
    my %State = %$StateP;
    my %Config = %$ConfigP; 

    $logger->info("Escalated to Defcon1");  

    # Create the name of this instance and set process title
    my $workerName=$Config{$globalSection}{$instanceName};
    $0 = "$programName-Defcon1-CRITICAL-" . $workerName;

    # Execute the Fencing Lock Commands

    foreach my $key ( keys %Config ) 
    {
        if ( $key =~ m/^app/ )
        {

            $logger->info(" === Fencing $key === ");

            my $fenceStr="port=".$Config{$key}{$dbport}
                        ." host=".$Config{$key}{$dbhost}
                        ." dbname=".$Config{$key}{$pgbouncerDb};

            $logger->info("^^^ Not executing trigger, Stats mode is active ^^^") if ( $statsMode==1);
            if ( $dryRun != 1 ) 
            {
                my @lockQueries;
                foreach $db ( split(/,/,$Config{$key}{$fenceLockDbs}))
                {
                    push ( @lockQueries, $Config{$key}{$fenceLockCommand} . " " . $db );
                }

                my $isFenced=ExecuteQueries($fenceStr, 
                                            $Config{$key}{$pgbouncerDbUser}, 
                                            $Config{$key}{$pgbouncerDbPass},
                                            @lockQueries);

                $isFenced=ExecuteQueries($fenceStr, 
                                            $Config{$key}{$pgbouncerDbUser}, 
                                            $Config{$key}{$pgbouncerDbPass},
                                            split (/ /,$Config{$key}{$fenceMoveCommand}));
            }
            else
            {
                $logger->info("Dry run is set, not fencing: $key");
            }
        }
    }

    # Run the failover commands
    foreach my $command ( @FailoverCommands ) 
    { 
        $logger->info( "Executing FAILOVER Command: " . $command . "\n");
        $logger->info("^^^ Not executing trigger, Stats mode is active ^^^") if ( $statsMode==1);
        if ( $dryRun != 1 ){
            my $ret=system($command);
        }
    }

    # Touch the trigger file
    $logger->info(" ==== PROMOTING STANDBY ==== ");
    $logger->info("^^^ Not executing trigger, Stats mode is active ^^^") if ( $statsMode==1);
    my $triggerCmdRet=0;
    if ( $dryRun != 1 ) 
    {
        $triggerCmdRet=system($triggerCommand);    
        if ( $triggerCmdRet != 0 ) 
        {
            my $msg="ERROR, Failoverd is unable to create the trigger file.  Failover cannot succeed.";
            print $msg . "\n";
            $logger->error($msg); 
        }
    }

    # Execute the Fencing UnLock Commands
    foreach my $key ( keys %Config ) 
    {
        if ( $key =~ m/^app/ )
        {

            $logger->info(" ==== Un-Fencing $key ==== ");

            my $fenceStr="port=".$Config{$key}{$dbport}
                        ." host=".$Config{$key}{$dbhost}
                        ." dbname=".$Config{$key}{$pgbouncerDb};

            $logger->info("^^^ Not executing FenceQueries, Stats mode is active ^^^") if ( $statsMode==1);

            if ( $dryRun != 1 ) 
            {
                my @unlockQueries;
                $logger->debug("Moving in for fence");
                foreach $db ( split(/,/,$Config{$key}{$fenceLockDbs}))
                {
                    push ( @unlockQueries, $Config{$key}{$fenceUnlockCommand} . " " . $db );
                }

                $logger->debug("Ready to exec: " . @unlockQueries);
                my $isUnfenced=ExecuteQueries($fenceStr, 
                                              $Config{$key}{$pgbouncerDbUser}, 
                                              $Config{$key}{$pgbouncerDbPass},
                                              @unlockQueries);
                $logger->debug("Completed unlock");
            }
        }
    }
    $logger->debug("Moving to slave verification");
    $logger->info("^^^ Not executing trigger, Stats mode is active ^^^") if ( $statsMode==1);
    if ( $dryRun != 1 )
    {
        $logger->debug("Prepping to check slave");
        my $slaveStr="port=".$Config{$slaveSection}{$dbport}
                    ." host=".$Config{$slaveSection}{$dbhost}
                    ." dbname=".$Config{$slaveSection}{$dbname};
        $logger->debug("Getting slave delay");
#        my %SlaveDelay=CalculateSlaveDelay($Config);
        $logger->debug("Complete slave delay Calc");
        my $slaveVerificationQuery = "INSERT INTO " . $Config{$masterSection}{$statusTable} 
                   . " (dttm,event,properties) VALUES (now(),'FAILOVER',"
                   . "'DelayTime:".$SlaveDelay{'time_delay'} . ",DelayKB:".$SlaveDelay{'replay_delay'}."')";

        my @slaveVerification=($slaveVerificationQuery);
        my $slaveUp=0; 
        while ( $slaveUp != 1 )
        {
            $logger->debug("Executing ". $slaveVerification[0]." to check slave");
            my $isConnected=ExecuteQueries($slaveStr, 
                                            $Config{$slaveSection}{$dbuser}, 
                                            $Config{$slaveSection}{$dbpass},
                                            @slaveVerification);
            $logger->debug("Query complete");
            if ( $isConnected == 1 ) 
            {
                $logger->info("Slave is now promoted");
                $slaveUp=1;       
            }
            else 
            {
                $logger->info("Slave is being promoted");
            }
            sleep(2);
        }
    }
    $logger->info("Failoverd shutting down");
   
    if ( $statsMode != 1 ) 
    { 
        exit(1);
    }


} # End: sub Defcon1

# vim: autoindent ts=4 sw=4 expandtab
